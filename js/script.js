//First approach to getting list

// const someArray = (arr) => {
//     const list = document.createElement('ul');
//     document.body.appendChild(list);
//
//     arr.map(item => {
//         list.innerHTML +=`<li>${item}</li>`;
//     });
// };
// someArray(['1', '2', '3', 'sea', 'user', 23]);

//Second approach to getting list
const someArray = (arr) => {
    const list = document.createElement('ul');
    document.body.appendChild(list);

    arr.map(item => {
        const listItem = document.createElement('li');
        list.appendChild(listItem).innerText = `${item}`;
    });

};
someArray(['1', '2', '3', 'sea', 'user', 23]);





